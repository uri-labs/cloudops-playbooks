# Ansible Playbooks



## AWS



aws ssm send-command \
--document-name "AWS-RunAnsiblePlaybook" \
--document-version "1" --targets '[{"Key":"tag:Name","Values":["u2536-cron-server"]}]' \
--parameters '{"playbook":[""],"playbookurl":["https://bitbucket.org/uri-labs/cloudops-playbooks/raw/3master/install-nginx.yml"],"extravars":["SSM=True"],"check":["False"],"timeoutSeconds":["3600"]}' --timeout-seconds 600 --max-concurrency "50" \
--max-errors "0" \
--cloud-watch-output-config '{"CloudWatchOutputEnabled":true,"CloudWatchLogGroupName":"cloudops-lab-logs"}' \
--region us-west-2


